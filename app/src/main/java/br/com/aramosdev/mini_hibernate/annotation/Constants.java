package br.com.aramosdev.mini_hibernate.annotation;

/**
 * @author Alberto Ramos
 * Created by programador on 28/01/15.
 */
public @interface Constants {
    static String URL_SET_DATA_USER = "";
    static final int SENDS_RESPOSTAS_TIME_OUT = 5000;
}
