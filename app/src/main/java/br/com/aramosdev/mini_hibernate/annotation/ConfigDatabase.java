package br.com.aramosdev.mini_hibernate.annotation;

import java.util.ArrayList;
import java.util.List;

import br.com.aramosdev.mini_hibernate.entity.User;

/**
 * Created by programador on 27/01/15.
 */
public @interface ConfigDatabase {
    int version = 28;
    String name = "MiniHibernate";
    List<Class> table = new ArrayList<Class>(){{
        add(User.class);
    }};
}
