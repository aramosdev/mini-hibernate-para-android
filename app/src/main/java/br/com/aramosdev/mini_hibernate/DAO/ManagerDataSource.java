package br.com.aramosdev.mini_hibernate.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import android.database.SQLException;
import android.util.Log;

import br.com.aramosdev.mini_hibernate.annotation.Autoincrement;
import br.com.aramosdev.mini_hibernate.annotation.Field;
import br.com.aramosdev.mini_hibernate.annotation.Id;
import br.com.aramosdev.mini_hibernate.util.Util;

/**
 * @author Alberto Ramos
 * Created by programador on 27/01/15.
 */
public class ManagerDataSource {
    private Class[] noparams = {};
    private Class[] param = new Class[1];
    private SQLiteDatabase database;
    private EntityManager entityManager;
    private String[] fields;
    private int id;

    public ManagerDataSource(Context context) {
        this.entityManager = new EntityManager(context);
    }
    public void open() throws SQLException {
        this.database = this.entityManager.getWritableDatabase();
    }
    public void close() {
        this.entityManager.close();
    }
    public void insert(Object obj) throws Exception {
        ContentValues values = new ContentValues();
        java.lang.reflect.Field[] fields = obj.getClass().getDeclaredFields();
        for (int i = 0; i < fields.length ; i++) {
            if (fields[i].isAnnotationPresent(Field.class)) {
                if (!fields[i].isAnnotationPresent(Autoincrement.class)){
                    Method method = obj.getClass().getDeclaredMethod("get" + Util.firstUpperCase(fields[i].getName()),
                            this.noparams);
                    values.put(fields[i].getName(), String.valueOf(method.invoke(obj, null)));
                }
            }
        }
        String table = this.entityManager.getTable(obj.getClass());
        Log.d("Num Coluna insert", String.valueOf(this.entityManager.getWritableDatabase().insert(table, null, values)));
    }
    public List<?> getAll(Object entity) throws Exception {
        List<Object> ListObj = new ArrayList<>();
        String table = this.entityManager.getTable(entity.getClass());
        this.getFieldsAll(entity.getClass().getAnnotations());
        Cursor cursor = this.database.query(table, null, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Object obj = this.cursorToObject(cursor, entity.getClass());
            ListObj.add(obj);
            cursor.moveToNext();
        }
        cursor.close();
        return ListObj;
    }
    public Object getObjectFromId(int id, Object entity) throws Exception {
        Object obj = null;
        String table = this.entityManager.getTable(entity.getClass());
        this.getFieldsAll(entity.getClass().getAnnotations());
        String fieldId = getFieldId(entity.getClass().getDeclaredFields());
        Cursor cursor = this.database.query(table,null,fieldId+"=?",new String[]{String.valueOf(id)},null,null,null,null);
        cursor.moveToFirst();
        if(cursor ==null || cursor.getCount() == 0){
            return null;
        }
        obj = this.cursorToObject(cursor, entity.getClass());
        return obj;
    }
    public void deleteRow(Object entity) throws Exception {
        String table = this.entityManager.getTable(entity.getClass());
        String fieldId = this.getFieldId(entity.getClass().getDeclaredFields());
        int valueId = this.getValueId(entity);
        String where = fieldId+"='"+valueId+"'";
        Log.d("Num Coluna afetada", String.valueOf(this.database.delete(table, where, null)));
    }
    public void truncate(Object obj){
        String table = this.entityManager.getTable(obj.getClass());
        this.entityManager.getWritableDatabase().delete(table, null, null);
    }
    public void updateValuesObject(Object entity) throws Exception {
        Class cls = entity.getClass();
        String table = this.entityManager.getTable(entity.getClass());
        String fieldId = getFieldId(cls.getDeclaredFields());
        ContentValues values = this.createObjectContentValues(entity);
        String[] args = {String.valueOf(this.id)};
        Log.d("Num Coluna afetada", String.valueOf(this.database.update(table, values, fieldId+"=?", args)));
    }
    private ContentValues createObjectContentValues(Object entity) throws Exception {
        ContentValues values = new ContentValues();
        Class[] noparams = {};
        Class cls = entity.getClass();
        this.getFieldsAll(entity.getClass().getAnnotations());
        java.lang.reflect.Field[] declaredFields = entity.getClass().getDeclaredFields();
        for (java.lang.reflect.Field field : declaredFields) {
            Method method = cls.getMethod("get" + Util.firstUpperCase(field.getName()), noparams);
            if (field.isAnnotationPresent(Id.class)){
                this.id = (int) method.invoke(entity,null);
            }else{
                values.put(field.getAnnotation(Field.class).name(), String.valueOf(method.invoke(entity,null)));
            }
        }
        return values;
    }
    private void getFieldsAll(Annotation[] annotations){
       this.fields = new String[annotations.length];
        for (int i = 0; i < annotations.length; i++)
            if (annotations[i] instanceof Field)
                this.fields[i] = ((Field)annotations[i]).name();
    }
    private String getFieldId(java.lang.reflect.Field[] fields){
        for (int i = 0; i < fields.length; i++)
            if (fields[i].isAnnotationPresent(Id.class))
                return fields[i].getAnnotation(Field.class).name();
        return "";
    }
    private int getValueId(Object entity) throws Exception {
        Class[] noparams = {};
        Class cls = entity.getClass();
        java.lang.reflect.Field[] fields = cls.getDeclaredFields();
        for (int i = 0; i < fields.length; i++)
            if (fields[i].isAnnotationPresent(Id.class)){
                Method method = cls.getMethod("get" + Util.firstUpperCase(fields[i].getName()), noparams);
                return (int) method.invoke(entity,null);
            }
        return 0;
    }
    private Object cursorToObject(Cursor cursor,Class cls) throws Exception{
        Object obj = cls.newInstance();
        for (int i = 0; i < cursor.getColumnCount(); i++) {
            java.lang.reflect.Field declaredField = cls.getDeclaredField(cursor.getColumnName(i));
            if (declaredField.getType().isPrimitive()){
                this.param[0] = Integer.TYPE;
                Method methodSetFilia = obj.getClass().getDeclaredMethod("set" + Util.firstUpperCase(cursor.getColumnName(i)),
                        this.param);
                methodSetFilia.invoke(obj, cursor.getInt(i));
            }else{
                this.param[0] = String.class;
                Method methodSetFilia = obj.getClass().getDeclaredMethod("set" + Util.firstUpperCase(cursor.getColumnName(i)),
                        this.param);
                methodSetFilia.invoke(obj, cursor.getString(i));
            }
        }
        return obj;
    }
}
