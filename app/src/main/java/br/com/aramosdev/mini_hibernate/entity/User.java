package br.com.aramosdev.mini_hibernate.entity;

import java.io.Serializable;

import br.com.aramosdev.mini_hibernate.annotation.Autoincrement;
import br.com.aramosdev.mini_hibernate.annotation.Field;
import br.com.aramosdev.mini_hibernate.annotation.FieldsRequired;
import br.com.aramosdev.mini_hibernate.annotation.Id;
import br.com.aramosdev.mini_hibernate.annotation.Table;

/**
 * @author Alberto Ramos
 * Created by programador on 04/03/15.
 */
@Table(name="user")
public class User implements Serializable {
    @Id
    @Autoincrement
    @Field(name="id_user",type = "integer")
    private int id_user;
    @Field(name="login",type = "text")
    @FieldsRequired
    private String login;
    @Field(name="senha",type = "text")
    @FieldsRequired
    private String senha;
    @Field(name="cpf",type = "text")
    @FieldsRequired
    private String cpf;
    @Field(name="status",type = "integer")
    private int status;
    @Field(name="telefone",type = "text")
    private String telefone;

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }
}
