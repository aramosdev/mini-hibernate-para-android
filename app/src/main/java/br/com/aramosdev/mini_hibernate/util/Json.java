package br.com.aramosdev.mini_hibernate.util;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

//import org.apache.http.HttpEntity;
//import org.apache.http.HttpResponse;
//import org.apache.http.NameValuePair;
//import org.apache.http.client.HttpClient;
//import org.apache.http.client.entity.UrlEncodedFormEntity;
//import org.apache.http.client.methods.HttpPost;
//import org.apache.http.impl.client.DefaultHttpClient;
//import org.apache.http.message.BasicNameValuePair;
//import org.apache.http.util.EntityUtils;

/**
 * Created by programador on 14/01/15.
 * @author Alberto Ramos
 */
public abstract class Json{

    private static Class[] noparams = {};
    private static Class[] param = new Class[1];


    public static AsyncTask<String, Void, String> httpAsyncTask(final Object objectReturn,
                                                                final Object obj,
                                                                final String nameMethodExecFinalPost){
        return new AsyncTask<String, Void, String>() {
            @Override
            protected String doInBackground(String... urls) {
                return Json.POST(urls[0], obj);
            }
            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                try {
                    Json.param[0] = String.class;
                    Method method = objectReturn.getClass().getMethod(nameMethodExecFinalPost,Json.param);
                    method.invoke(objectReturn,result);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }
    public static String POST(String url, Object obj)  {
        String result = null;
        Log.d("Url", url);
        long startTime = System.currentTimeMillis();
        try {
            URL newUrl = new URL(url);
            HttpURLConnection connResquest=(HttpURLConnection)newUrl.openConnection();
            connResquest.setReadTimeout(100000);
            connResquest.setConnectTimeout(15000);
            connResquest.setRequestMethod("POST");
            connResquest.setDoInput(true);
            connResquest.setDoOutput(true);
            OutputStream os = connResquest.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os,"UTF-8"));
            writer.write(Json.getParans(obj));
            writer.flush();
            writer.close();
            os.close();
            connResquest.connect();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        return "";
    }
    public static String readJsonString(String json,String field) throws JSONException {
        JSONObject mainObject = new JSONObject(json);
        return mainObject.getString(field).toString();
    }
    public static String getParans(Object obj) throws InvocationTargetException, IllegalAccessException, UnsupportedEncodingException, NoSuchMethodException {
        Field[] fields = obj.getClass().getDeclaredFields();
        StringBuffer result = new StringBuffer();
        boolean first = true;
        for (int i = 0; i < fields.length ; i++) {
            if (first)
                first = false;
            else
                result.append("&");
            Method method = null;
            method = obj.getClass()
                    .getDeclaredMethod("get" + Util.firstUpperCase(fields[i].getName()), Json.noparams);
            result.append(URLEncoder.encode(fields[i].getName(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(String.valueOf(method.invoke(obj, null)), "UTF-8"));
        }
        return result.toString();
    }
}
