package br.com.aramosdev.mini_hibernate.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.InputMismatchException;

/**
 * Created by programador on 13/01/15.
 */
public abstract class Util {
    public static String estadosUf(String estado){
        switch (estado){
            case "Acre":
                return "AC";
            case "Alagoas":
                return "AL";
            case "Amapá":
                return "AP";
            case "Amazonas":
                return "AM";
            case "Bahia":
                return "BA";
            case "CE":
                return "CE";
            case "Distrito Federal":
                return "DF";
            case "Espirito Santo":
                return "ES";
            case "Goiás":
                return "GO";
            case "Maranhão":
                return "MA";
            case "Mato Grosso":
                return "MT";
            case "Mato Grosso do Sul":
                return "MS";
            case "Minas Gerais":
                return "MG";
            case "Paraná":
                return "PR";
            case "Paraíba":
                return "PB";
            case "Pará":
                return "PA";
            case "Pernambuco":
                return "PE";
            case "Piauí":
                return "PI";
            case "Rio de Janeiro":
                return "RJ";
            case "Rio Grande do Norte":
                return "RN";
            case "Rio Grande do Sul":
                return "RS";
            case "Rondonia":
                return "RO";
            case "Roraima":
                return "RR";
            case "Santa Catarina":
                return "SC";
            case "Sergipe":
                return "SE";
            case "São Paulo":
                return "SP";
            case "Tocantins":
                return "TO";

        }
        return "";
    }
    public static boolean checkCpf(String cpf) {
        if (cpf.equals("00000000000")
                || cpf.equals("11111111111")
                || cpf.equals("22222222222")
                || cpf.equals("33333333333")
                || cpf.equals("44444444444")
                || cpf.equals("55555555555")
                || cpf.equals("66666666666")
                || cpf.equals("77777777777")
                || cpf.equals("88888888888")
                || cpf.equals("99999999999")
                || (cpf.length() != 11)) {

            return (false);
        }

        char dig10, dig11;
        int sm, i, r, num, peso;


        // "try" - protege o codigo para eventuais erros de conversao de tipo (int)
        try {
            // Calculo do 1o. Digito Verificador
            sm = 0;
            peso = 10;
            for (i = 0; i < 9; i++) {
                // converte o i-esimo caractere do CPF em um numero:
                // por exemplo, transforma o caractere '0' no inteiro 0
                // (48 eh a posicao de '0' na tabela ASCII)
                num = (int) (cpf.charAt(i) - 48);
                sm = sm + (num * peso);
                peso = peso - 1;
            }
            r = 11 - (sm % 11);
            if ((r == 10) || (r == 11)) {
                dig10 = '0';
            } else {
                dig10 = (char) (r + 48);
            }
            // converte no respectivo caractere numerico
            // Calculo do 2o. Digito Verificador
            sm = 0;
            peso = 11;
            for (i = 0; i < 10; i++) {
                num = (int) (cpf.charAt(i) - 48);
                sm = sm + (num * peso);
                peso = peso - 1;
            }
            r = 11 - (sm % 11);
            if ((r == 10) || (r == 11)) {
                dig11 = '0';
            } else {
                dig11 = (char) (r + 48);
            }
            // Verifica se os digitos calculados conferem com os digitos informados.
            if ((dig10 == cpf.charAt(9)) && (dig11 == cpf.charAt(10))) {
                return (true);
            } else {
                return (false);
            }
        } catch (InputMismatchException erro) {
            return (false);
        }
    }

    public static String firstUpperCase(String text) {
        final StringBuilder result = new StringBuilder(text);
        int i = 0;
        do {
            result.replace(i, i + 1, result.substring(i,i + 1).toUpperCase());
            i =  result.indexOf(" ", i) + 1;
        } while (i > 0 && i < result.length());
        return result.toString();
    }
    public static String formatDate(String format, String date){
        Date DataNascFr = null;
        String DataNascDb = null;
        try{
            DataNascFr = new SimpleDateFormat("dd/MM/yyyy").parse(date);
            DataNascDb = new SimpleDateFormat("yyyy-MM-dd").format(DataNascFr);
        }catch( java.text.ParseException e ){
            e.printStackTrace();
        }
        return DataNascDb;
    }
    public static String[] addFieldsArrayString(String[] fields, String[] oldArray){
        String[] newArray = new String[oldArray.length + fields.length];
        int aux = 0;
        for(int i=0;i< newArray.length;i++) {
            if (i >= oldArray.length ){
                newArray[i] = fields[aux];
                aux++;
            }else{
                newArray[i] = oldArray[i];
            }
        }
        return newArray;
    }
    public static byte[] getBytesFromFile(File file) throws IOException {
        InputStream stream = new FileInputStream(file);
        long size = file.length();
        if (size > Integer.MAX_VALUE) {
            return null;
        }
        byte[] bytes = new byte[(int)size];
        int offset = 0;
        int numRead;
        while (offset < bytes.length
                && (numRead=stream.read(bytes, offset, bytes.length-offset)) >= 0) {
            offset += numRead;
        }
        if (offset < bytes.length) {
            throw new IOException("Could not completely read file "+file.getName());
        }
        stream.close();
        return bytes;
    }
    public static String readTextFromResource(InputStream raw ) throws IOException {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        int i;
        i = raw.read();
        while (i != -1){
            stream.write(i);
            i = raw.read();
        }
        raw.close();
        return stream.toString();
    }
}
