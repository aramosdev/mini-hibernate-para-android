package br.com.aramosdev.mini_hibernate.model;

import android.content.Context;
import java.util.ArrayList;
import java.util.List;
import br.com.aramosdev.mini_hibernate.DAO.ManagerDataSource;
import br.com.aramosdev.mini_hibernate.entity.User;

/**
 * Created by programador on 17/03/15.
 */
public class ManageUser {
    private User user;
    private ManagerDataSource dataSource;
    private Context context;

    public ManageUser(Context context) {
        this.context = context;
        if (this.dataSource == null)
            this.dataSource = new ManagerDataSource(this.context);
        if (this.user == null)
            this.user = new User();
    }
    public User createUser(User user){
        this.user = user;
        this.dataSource.open();
        try {
            this.dataSource.insert(this.user);
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.dataSource.close();
        return this.user;
    }
    public List<User> ListUser(){
        this.dataSource.open();
        List<User> listUsers = null;
        try {
            listUsers = (ArrayList<User>) this.dataSource.getAll(this.user);
            if (listUsers == null)
                listUsers = new ArrayList<>();
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.dataSource.close();
        return listUsers;
    }
    public boolean truncate(User user){
        this.dataSource.open();
        this.dataSource.truncate(user);
        this.dataSource.close();
        return true;
    }
    public boolean isUser(int user_id){
        this.dataSource.open();
        try {
            if (this.dataSource.getObjectFromId(user_id, this.user) == null)
                return false;
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.dataSource.close();
        return true;
    }
}
