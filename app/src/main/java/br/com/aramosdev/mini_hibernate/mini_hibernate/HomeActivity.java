package br.com.aramosdev.mini_hibernate.mini_hibernate;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;

import br.com.aramosdev.mini_hibernate.R;

public class HomeActivity extends ActionBarActivity implements AdapterView.OnItemClickListener {
    public static final String HOME = "br.com.aramosdev.mini_hibernate.HOME_ACTIVITY";

    public HomeActivity() {

    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_app);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }
    @Override
    protected void onResume() {
        super.onResume();

    }

}
